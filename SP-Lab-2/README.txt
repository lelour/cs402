How to use:
Program will take one command line argument, which is the name of the
input file containing employee information.
For example, telling your program to load the employee data stored
in the file "small.txt" would look like:
./workerDB small.txt


Directory Architecture:
include : contains all header files
src : contains C file
src/module: contains menu, read file, dbop, stringop four module C files.


System Architecture:
Modules:
  1, "main.c" is the entrance of this project and will load the command line parameter, check exist and format and sort in memory. Also there's an entry for menu.
  2, "menu.c" and "menu.h" are the menu display and operation handler
  3, "readfile.c" and "readfile.h" will handle file related ops, like open file, close file, read records in database file
  4, "dbop.c" and "dbop.h" hold database related logic ops, like sort, print database, lookup id, lookup last name ,add record, remove record, update record, print top M highest salary employees and find all matched employees with last name in memory
  5, "stringop.h" and "stringop.c" contain string related functions, like stricmp

How to build:
make workersDB
make clean

After build, there will be a new file named workersDB.
To run it, just type:
./workersDB small.txt


Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
  (6) Remove an employee
  (7) Update an employee's information
  (8) Print the M employees with the highest salaries
  (9) Find all employees with matching last name
----------------------------------
Enter your choice: 6
Enter a 6 digit employee id to remove: 970016

NAME                              SALARY             ID
---------------------------------------------------------------
Russ         Vorobiev             109000         970016
---------------------------------------------------------------

Do you want to remove the above employee ?
Enter 1 for yes, other input for no: 1
Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
  (6) Remove an employee
  (7) Update an employee's information
  (8) Print the M employees with the highest salaries
  (9) Find all employees with matching last name
----------------------------------
Enter your choice: 1

NAME                              SALARY             ID
---------------------------------------------------------------
Cathryn      Danner                72000         165417
Matt         Meeden                69000         273225
Robert       Dufour                91000         471163
Dylan        Steinberg             84000         485913
Mike         Griffin               72000         499959
Daniel       McNamee               71000         547935
Peter        Olsen                 82000         553997
Martine      Marshall              99000         633976
Jean         Jones                 94000         702234
Dana         Parrish               87000         784372
Ann          Coddington            82000         786785
Melissa      Dufour               114000         849387
Heather      James                 98000         935460
---------------------------------------------------------------
 Number of Employees (13)

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
  (6) Remove an employee
  (7) Update an employee's information
  (8) Print the M employees with the highest salaries
  (9) Find all employees with matching last name
----------------------------------
Enter your choice: 7
Enter a 6 digit employee id to update: 553997

NAME                              SALARY             ID
---------------------------------------------------------------
Peter        Olsen                 82000         553997
---------------------------------------------------------------

 Select which field you want to update :
Enter 1 for firstname, 2 for lastname, 3 for salary, 4 for all , other for no update :1
Please enter new first name of this employee : Peterson
Do you want to update the above employee ?
        Peterson Olsen, salary: 82000
Enter 1 for yes, other input for no: 1
Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
  (6) Remove an employee
  (7) Update an employee's information
  (8) Print the M employees with the highest salaries
  (9) Find all employees with matching last name
----------------------------------
Enter your choice: 1

NAME                              SALARY             ID
---------------------------------------------------------------
Cathryn      Danner                72000         165417
Matt         Meeden                69000         273225
Robert       Dufour                91000         471163
Dylan        Steinberg             84000         485913
Mike         Griffin               72000         499959
Daniel       McNamee               71000         547935
Peterson     Olsen                 82000         553997
Martine      Marshall              99000         633976
Jean         Jones                 94000         702234
Dana         Parrish               87000         784372
Ann          Coddington            82000         786785
Melissa      Dufour               114000         849387
Heather      James                 98000         935460
---------------------------------------------------------------
 Number of Employees (13)

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
  (6) Remove an employee
  (7) Update an employee's information
  (8) Print the M employees with the highest salaries
  (9) Find all employees with matching last name
----------------------------------
Enter your choice: 8
Enter the value of M employees with highest salaries: 5

NAME                              SALARY             ID
---------------------------------------------------------------
Melissa      Dufour               114000         849387
Martine      Marshall              99000         633976
Heather      James                 98000         935460
Jean         Jones                 94000         702234
Robert       Dufour                91000         471163
---------------------------------------------------------------

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
  (6) Remove an employee
  (7) Update an employee's information
  (8) Print the M employees with the highest salaries
  (9) Find all employees with matching last name
----------------------------------
Enter your choice: 9
Enter Employee's last name (no extra spaces): Dufour

NAME                              SALARY             ID
---------------------------------------------------------------
Robert       Dufour                91000         471163
Melissa      Dufour               114000         849387
---------------------------------------------------------------

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
  (6) Remove an employee
  (7) Update an employee's information
  (8) Print the M employees with the highest salaries
  (9) Find all employees with matching last name
----------------------------------
Enter your choice: 5
goodbye!
