//
// Created by Jian Li on 2021/3/9.
// Purpose:
// Ths project is to implement an employee database to store and query.
//
// How to use:
// Program will take one command line argument, which is the name of the
// input file containing employee information.
// For example, telling your program to load the employee data stored
// in the file "small.txt" would look like:
//./workerDB small.txt
//
//Modules:
// main.c is the entrance of this project and will load the command line parameter, check exist and format and sort in memory. Also there's an entry for menu.
// menu.c is the menu operation handler
// readfile.c will handle file related ops, like open file, close file, read records in database file
// dbop.c holds database related logic ops, like sort, print database, lookup id, lookup last name and add record in memory
//
#include <stdio.h>
#include "../include/readfile.h"
#include "../include/menu.h"
#include "../include/dbop.h"

struct EMPLOYEE employee[MAXEMPLOYEE]; // array to hold all records
int amount;  // amount of employee

// main entry of this program
int main(int argc, char *argv[]) {
    char *dbfile;
    // dbfile = "small.txt";   // for test to obtain dbfile

    // get parameter as database filename
    if (argc != 2) {
        printf("The command format should be (e.g.) : ./workersDB small.txt\n");
        return -1;
    } else {
        dbfile = argv[1];
    }

    // read records in dbfile
    // ret:
    // 0 success, -1 not exist, -2 format error
    int ret = read_file(dbfile);

    if (ret == -1) {
        printf("%s does not exist!\n", dbfile);
        return -1;
    }
    if (ret == -2) {
        printf("%s format is incorrect! Please verify.\n", dbfile);
        return -1;
    }

    // sort by ID in memory
    sort_by_ID();

    // menu operation
    init_menu();
    do {
        display_menu();
        do_menu(dbfile);
    }while(0 == 0);

    return 0;
}

