//
// Created by Jian Li on 2021/3/10.
//
// File operation:
// dbfile exist, readfile, readfile by id, readfile by lastname, add
//
#include <stdio.h>
#include <string.h>
#include "../../include/readfile.h"

extern struct EMPLOYEE employee[MAXEMPLOYEE]; // array to hold all records
extern int amount;  // amount of employee

// local viarable used in this library
FILE *fp;

// open file with filename and mode
int open_file(char *filename) {
    fp = fopen(filename, "r");
    if (fp != NULL) {
        return 0;  // exist
    } else {
        return -1; // not exist
    }
}

// close file handler
int close_file() {
    fclose(fp);
    return 0;
}


// read field with specified position and check range
// return: >0 - correct; -1 - incorrect
int read_int(int *data) {
    int result = fscanf(fp, "%d", data);
    if (*data>=100000 && *data<=999999) {
        return 0;
    } else {
        return -1;  // out of range
    }
}

// read field with specified position
// return: 0 - correct, -1 - incorrect
int read_string(char *data) {
    int result = fscanf(fp, "%s", data);
    if (strlen(data) != 0)
        return 0;
    else
        return -1;  // null
}

// read field with specified position and check range
// return: >0 - correct, -1 - incorrect
int read_salary(int *data) {
    int result = fscanf(fp, "%d", data);
    if (*data>=30000 && *data<=150000) {
        return 0;
    } else {
        return -1;  // out of range
    }
}

// read records in database file
// format should be four columns matches struct employee
// data parsed into global static struct array employee
// return:
// -1  file not exist
// -2  file format mismatch
//  0  success
int read_file(char *filename) {
    int ret;
    struct EMPLOYEE current_emp; // current employee
    char buf[MAXLINELENGTH];  // buffer to read line, include \n
    int index = 0;
    int ID;
    char firstname[MAXNAME];
    char lastname[MAXNAME];
    int salary;

    ret = open_file(filename);
    if (ret == -1) return -1;  // file not exist
    while (!feof(fp)) {
        ret = read_int(&ID);
        if (ret == -1) return -2;  // file format mismatch
        ret = read_string(firstname);
        if (ret == -1) return -2;  // file format mismatch
        ret = read_string(lastname);
        if (ret == -1) return -2;  // file format mismatch
        ret = read_salary(&salary);
        if (ret == -1) return -2;  // file format mismatch
        employee[index].six_digit_ID = ID;
        strcpy(employee[index].first_name, firstname);
        strcpy(employee[index].last_name, lastname);
        employee[index].salary = salary;
        index ++;
    }
    close_file();
    amount = index;  // save global amount of employee

    return 0; // 0 - success
}

