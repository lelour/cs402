//
// Created by Jian Li on 2021/3/16.
//
// database related ops
// split, read_int, read_string, read_float, read_record

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../include/readfile.h"
#include "../../include/dbop.h"
#include "../../include/stringop.h"

extern struct EMPLOYEE employee[MAXEMPLOYEE]; // array to hold all records
extern int amount;  // amount of employee

// sort by ID in struct array
// return: 0 - success
int sort_by_ID() {
    struct EMPLOYEE tmp_emp;  // for swap
    // Bubble Sort by ID
    for (int i=0; i<amount-1; i++) {
        for (int j=0; j<amount-1-i; j++) {
            if (employee[j].six_digit_ID > employee[j+1].six_digit_ID) {
                tmp_emp = employee[j];
                employee[j] = employee[j+1];
                employee[j+1] = tmp_emp;
            }
        }
    }

    return 0;
}

// print all data of database file
// print format should be four columns matches lab demands
int print_database() {

    printf("\n");
    printf("NAME                              SALARY             ID\n");
    printf("---------------------------------------------------------------\n");
    for (int i=0; i<amount; i++) {
        printf("%-13s", employee[i].first_name);
        printf("%-21s", employee[i].last_name);
        printf("%6d", employee[i].salary);
        printf("%15d", employee[i].six_digit_ID);
        printf("\n");
    }
    printf("---------------------------------------------------------------\n");
    printf(" Number of Employees (%d)\n\n", amount);

    return 0; // 0 - success
}


// lookup record with id in memory
// when found there is only one result
// print format should be four columns matches lab demands
int lookup_id(int input_id) {
    struct EMPLOYEE current_emp; // current employee
    int found = 0;  // 0 - not found, 1 - found

    // linear search
    for (int i=0; i<amount; i++) {
        if (employee[i].six_digit_ID == input_id) {
            current_emp = employee[i];
            found = 1;
            break;
        }
    }

    if (found == 0) {
        printf("Employee with id %d not found in DB\n\n", input_id);
    } else {
        printf("\n");
        printf("NAME                              SALARY             ID\n");
        printf("---------------------------------------------------------------\n");
        printf("%-13s", current_emp.first_name);
        printf("%-21s", current_emp.last_name);
        printf("%6d", current_emp.salary);
        printf("%15d", current_emp.six_digit_ID);
        printf("\n");
        printf("---------------------------------------------------------------\n\n");

    }

    return 0; // 0 - success
}

// lookup record with lastname in memory
// when found print the first one
// print format should be four columns matches lab demands
int lookup_lastname(char *input_name) {
    struct EMPLOYEE current_emp; // current employee
    int found = 0;  // 0 - not found, 1 - found

    for (int i=0; i<amount; i++) {
        if (strcmp(employee[i].last_name, input_name) == 0) {
            current_emp = employee[i];
            found = 1;
            break;
        }
    }

    if (found == 0) {
        printf("Employee with lastname %s not found in DB\n\n", input_name);
    } else {
        printf("\n");
        printf("NAME                              SALARY             ID\n");
        printf("---------------------------------------------------------------\n");
        printf("%-13s", current_emp.first_name);
        printf("%-21s", current_emp.last_name);
        printf("%6d", current_emp.salary);
        printf("%15d", current_emp.six_digit_ID);
        printf("\n");
        printf("---------------------------------------------------------------\n\n");
    }

    return 0; // 0 - success
}

// append record to EOF in memory
int add_record(char *firstname, char *lastname, int salary) {

    int biggest_id = 0;

    // find the biggest ID
    for (int i=0; i<amount; i++) {
        if (employee[i].six_digit_ID > biggest_id) {
            biggest_id = employee[i].six_digit_ID;
        }
    }

    // ID must be in range, or it would not append record
    biggest_id++;  // id = biggest_id + 1
    if (biggest_id > 999999) {
        printf("Index is out of range [100000 - 999999]!\n");
        return -1;  // return fail
    }

    if (biggest_id < 100000) biggest_id = 100000;  // set to the least id of range when no data in file

    employee[amount].six_digit_ID = biggest_id;
    strcpy(employee[amount].first_name, firstname);
    strcpy(employee[amount].last_name, lastname);
    employee[amount].salary = salary;
    amount++;

    printf("\n");

    return 0; // 0 - success, -1 - fail
}

// remove record with id in memory
// when found there is only one result
// confirm whether make sure to remove
// do remove if yes
int remove_id(int input_id) {
    int found = 0;  // 0 - not found, 1 - found
    int index = 0;  // indicate index of found record
    int choice; // remove ensure

    // linear search
    for (int i=0; i<amount; i++) {
        if (employee[i].six_digit_ID == input_id) {
            found = 1;
            break;
        }
        index++;  // increase index pointer
    }

    if (found == 0) {
        printf("Employee with id %d not found in DB\n\n", input_id);
    } else {
        printf("\n");
        printf("NAME                              SALARY             ID\n");
        printf("---------------------------------------------------------------\n");
        printf("%-13s", employee[index].first_name);
        printf("%-21s", employee[index].last_name);
        printf("%6d", employee[index].salary);
        printf("%15d", employee[index].six_digit_ID);
        printf("\n");
        printf("---------------------------------------------------------------\n\n");
        printf("Do you want to remove the above employee ?\n");
        printf("Enter 1 for yes, other input for no: ");
        scanf("%d", &choice);
        if (choice == 1) {
            // move records
            for (int i=index; i<amount-1; i++) {
                employee[i] = employee[i+1];
            }
            // clear last one
            employee[amount-1].six_digit_ID = 0;
            strcpy(employee[amount-1].first_name, "");
            strcpy(employee[amount-1].last_name, "");
            employee[amount-1].salary = 0;
            // counter -1
            amount--;
        }
    }

    return 0; // 0 - success
}

// update record with id in memory
// when found there is only one result
// user can choose which field or all fields to update
// confirm whether make sure to update
// do update if yes
int update_id(int input_id) {
    int found = 0;  // 0 - not found, 1 - found
    int index = 0;  // indicate index of found record
    int field_choice; // update field
    int choice; // update ensure
    char new_firstname[MAXNAME];
    char new_lastname[MAXNAME];
    int new_salary;
    int first_time_input;

    // linear search
    for (int i=0; i<amount; i++) {
        if (employee[i].six_digit_ID == input_id) {
            found = 1;
            break;
        }
        index++;  // increase index pointer
    }

    if (found == 0) {
        printf("Employee with id %d not found in DB\n\n", input_id);
    } else {
        printf("\n");
        printf("NAME                              SALARY             ID\n");
        printf("---------------------------------------------------------------\n");
        printf("%-13s", employee[index].first_name);
        printf("%-21s", employee[index].last_name);
        printf("%6d", employee[index].salary);
        printf("%15d", employee[index].six_digit_ID);
        printf("\n");
        printf("---------------------------------------------------------------\n\n");
        printf(" Select which field you want to update : \nEnter 1 for firstname, 2 for lastname, 3 for salary, 4 for all , other for no update :");
        scanf("%d", &field_choice);
        first_time_input = 0;
        switch (field_choice) {
            case 1:  // update firstname
                printf("Please enter new first name of this employee : ");
                scanf("%s", new_firstname);
                break;
            case 2:  // update lastname
                printf("Please enter new last name of this employee : ");
                scanf("%s", new_lastname);
                break;
            case 3:  // update salary
                do {
                    if (first_time_input == 1) printf("Please check you input!\n");
                    printf("Enter employee's new salary (30000 to 150000): ");
                    scanf("%d", &new_salary);
                    if ((new_salary<30000) || (new_salary>150000)) first_time_input=1;
                } while(new_salary<30000 || new_salary>150000);
                break;
            case 4:  // update all
                printf("Please enter new first name of this employee : ");
                scanf("%s", new_firstname);
                printf("Please enter new last name of this employee : ");
                scanf("%s", new_lastname);
                do {
                    if (first_time_input == 1) printf("Please check you input!\n");
                    printf("Enter employee's new salary (30000 to 150000): ");
                    scanf("%d", &new_salary);
                    if ((new_salary<30000) || (new_salary>150000)) first_time_input=1;
                } while(new_salary<30000 || new_salary>150000);
                break;
            default:
                return -1;   // quit update
                break;
        }
        printf("Do you want to update the above employee ?\n");
        if (field_choice == 1) {
            printf("        %s %s, salary: %d\n", new_firstname, employee[index].last_name, employee[index].salary);
        } else if (field_choice == 2) {
            printf("        %s %s, salary: %d\n", employee[index].first_name, new_lastname, employee[index].salary);
        } else if (field_choice == 3) {
            printf("        %s %s, salary: %d\n", employee[index].first_name, employee[index].last_name, new_salary);
        } else if (field_choice == 4) {
            printf("        %s %s, salary: %d\n", new_firstname, new_lastname, new_salary);
        }
        printf("Enter 1 for yes, other input for no: ");
        scanf("%d", &choice);
        if (choice == 1) {
            // update
            switch (field_choice) {
                case 1:
                    strcpy(employee[index].first_name, new_firstname);
                    break;
                case 2:
                    strcpy(employee[index].last_name, new_lastname);
                    break;
                case 3:
                    employee[index].salary = new_salary;
                    break;
                case 4:
                    strcpy(employee[index].first_name, new_firstname);
                    strcpy(employee[index].last_name, new_lastname);
                    employee[index].salary = new_salary;
                    break;
            }
        }
    }

    return 0; // 0 - success
}

// print top M highest salary employees
int M_highest_salary(int M) {
    struct top_salary head;  // a null head node
    struct top_salary *pointer;  // pointer to last node
    struct top_salary *tmp_pointer;
    struct top_salary temp;  // temp node
    int inserted; // indicated whether inserted, 0 - yes, 1 - no
    int index;

    pointer = &head;
    for (int i=0; i<amount; i++) {
        // first node
        if (pointer == &head) {
            pointer->next = (struct top_salary *)malloc(sizeof(struct top_salary));
            pointer = pointer->next;
            pointer->index = i;
            pointer->salary = employee[i].salary;
            pointer->next = NULL;
        } else {
            pointer = &head;
            inserted = 1;  // not inserted
            while (pointer->next != NULL) {
                if ((inserted != 0) && (employee[i].salary > pointer->next->salary)) {
                    //insert
                    tmp_pointer = (struct top_salary *)malloc(sizeof(struct top_salary));
                    tmp_pointer->index = i;
                    tmp_pointer->salary = employee[i].salary;
                    // insert pointer
                    tmp_pointer->next = pointer->next;
                    pointer->next = tmp_pointer;
                    inserted = 0;
                    pointer = pointer->next;
                } else {
                    pointer = pointer->next;
                }
            }
            // the least salary, not insert into the linklist yet
            if (inserted != 0) {
                pointer->next = (struct top_salary *)malloc(sizeof(struct top_salary));
                pointer = pointer->next;
                pointer->index = i;
                pointer->salary = employee[i].salary;
                pointer->next = NULL;
            }
        }
    }

    // print top M node
    printf("\n");
    printf("NAME                              SALARY             ID\n");
    printf("---------------------------------------------------------------\n");
    pointer = &head;
    for (int i=0; i<M; i++) {
        index = pointer->next->index;
        printf("%-13s", employee[index].first_name);
        printf("%-21s", employee[index].last_name);
        printf("%6d", employee[index].salary);
        printf("%15d", employee[index].six_digit_ID);
        printf("\n");
        pointer = pointer->next;
    }
    printf("---------------------------------------------------------------\n\n");

    // free space
    pointer = &head;
    pointer = pointer->next;
    while (pointer != NULL) {
        tmp_pointer = pointer;
        pointer = pointer->next;
        free(tmp_pointer);
    }
    return 0;
}

// lookup record with lastname in memory
// when found print all matched records
// print format should be four columns matches lab demands
int lookup_all_lastname(char *input_name) {
    struct EMPLOYEE tmp_employee[MAXEMPLOYEE];
    int found = 0;  // 0 - not found, 1 - found
    int index = 0;

    for (int i=0; i<amount; i++) {
        if (stricmp(employee[i].last_name, input_name) == 0) {
            tmp_employee[index] = employee[i];
            index++;
            found = 1;
        }
    }

    if (found == 0) {
        printf("Employee with lastname %s not found in DB\n\n", input_name);
    } else {
        printf("\n");
        printf("NAME                              SALARY             ID\n");
        printf("---------------------------------------------------------------\n");
        for (int i=0; i<index; i++) {
            printf("%-13s", tmp_employee[i].first_name);
            printf("%-21s", tmp_employee[i].last_name);
            printf("%6d", tmp_employee[i].salary);
            printf("%15d", tmp_employee[i].six_digit_ID);
            printf("\n");
        }
        printf("---------------------------------------------------------------\n\n");
    }

    return 0; // 0 - success
}