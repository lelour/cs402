//
// Created by Jian Li on 2021/3/10.
//
// Menu operation
//
#include <stdio.h>
#include <stdlib.h>
#include "../../include/menu.h"
#include "../../include/readfile.h"
#include "../../include/dbop.h"

extern struct EMPLOYEE employee[MAXEMPLOYEE]; // array to hold all records
extern int amount;  // amount of employee

// initialize the menu parameters
void init_menu() {
    f_menu.status = _f_main_;
    f_menu.buf_size = BUFFERSIZE;   // bytes get from stdio one time
    f_menu.buf = malloc(BUFFERSIZE);
}

// display menu
void display_menu() {
    switch(f_menu.status) {
        case _f_main_ :
            printf("Employee DB Menu:\n");
            printf("----------------------------------\n");
            printf("  (1) Print the Database\n");
            printf("  (2) Lookup by ID\n");
            printf("  (3) Lookup by Last Name\n");
            printf("  (4) Add an Employee\n");
            printf("  (5) Quit\n");
            printf("  (6) Remove an employee\n");
            printf("  (7) Update an employee's information\n");
            printf("  (8) Print the M employees with the highest salaries\n");
            printf("  (9) Find all employees with matching last name\n");
            printf("----------------------------------\n");
            printf("Enter your choice: ");
            break;
        case _lookup_id_ :
            printf("Enter a 6 digit employee id: ");
            break;
        case _lookup_last_name_ :
            printf("Enter Employee's last name (no extra spaces): ");
            break;
        case _add_ :
            printf("Enter the first name of the employee: ");
            break;
        case _quit_ :
            printf("goodbye!\n");
            exit(1);
            break;
        case _remove_ :
            printf("Enter a 6 digit employee id to remove: ");
            break;
        case _update_ :
            printf("Enter a 6 digit employee id to update: ");
            break;
        case _M_highest_salary_ :
            printf("Enter the value of M employees with highest salaries: ");
            break;
        case _all_matching_last_name_ :
            printf("Enter Employee's last name (no extra spaces): ");
            break;
        case _error_ :
            printf("Enter your choice: ");
            f_menu.status = _f_main_;
            break;
    }
}

// operations on menu choice
void do_menu(char *filename) {
    scanf("%s", f_menu.buf);
    // user input stored in buf
    switch (f_menu.status) {
        int choice;
        int input_id;
        char input_lastname[MAXNAME];
        int input_salary;
        int add_choice;
        int first_time_input;  // check input coorection
        int ret;
        int input_M; // value of M highest salary employees

        case _f_main_ :
            sscanf(f_menu.buf, "%d", &choice);
            if (choice == 1) {
                print_database(filename);
            } else if (choice == 2) {
                f_menu.status = _lookup_id_;
            } else if (choice == 3) {
                f_menu.status = _lookup_last_name_;
            } else if (choice == 4) {
                f_menu.status = _add_;
            } else if (choice == 5) {
                f_menu.status = _quit_;
            } else if (choice == 6) {
                f_menu.status = _remove_;
            } else if (choice == 7) {
                f_menu.status = _update_;
            } else if (choice == 8) {
                f_menu.status = _M_highest_salary_;
            } else if (choice == 9) {
                f_menu.status = _all_matching_last_name_;
            } else {
                printf("Hey, %d is not between 1 and 9, try again...\n", choice);
                f_menu.status = _error_;
            }
            break;
        case _lookup_id_ :
            sscanf(f_menu.buf, "%d", &input_id);
            lookup_id(input_id);
            f_menu.status = _f_main_;
            break;
        case _lookup_last_name_ :
            lookup_lastname(f_menu.buf);
            f_menu.status = _f_main_;
            break;
        case _add_ :
            printf("Enter the last name of the employee: ");
            scanf("%s", input_lastname);
            first_time_input = 0;
            // salary must be in range
            do {
                if (first_time_input == 1) printf("Please check you input!\n");
                printf("Enter employee's salary (30000 to 150000): ");
                scanf("%d", &input_salary);
                if ((input_salary<30000) || (input_salary>150000)) first_time_input=1;
            } while(input_salary<30000 || input_salary>150000);
            printf("do you want to add the following employee to the DB?\n");
            printf("        %s %s, salary: %d\n", f_menu.buf, input_lastname, input_salary);
            printf("Enter 1 for yes, 0 for no: ");
            scanf("%d", &add_choice);
            if (add_choice == 1) {
                ret = add_record(f_menu.buf, input_lastname, input_salary);
                if (ret == -1) {
                    printf("Add record fail because ID cound not bigger than 999999!\n");
                }
            }
            f_menu.status = _f_main_;
            break;
        case _quit_:
            break;
        case _remove_:
            // remove a record
            sscanf(f_menu.buf, "%d", &input_id);
            remove_id(input_id);
            f_menu.status = _f_main_;
            break;
        case _update_:
            // update a record
            sscanf(f_menu.buf, "%d", &input_id);
            update_id(input_id);
            f_menu.status = _f_main_;
            break;
        case _M_highest_salary_:
            // M highest salaries records
            sscanf(f_menu.buf, "%d", &input_M);
            do {
                if (input_M<=0) {
                    printf("M must >0\n");
                    printf("PLease re-input M: ");
                    scanf("%d", &input_M);
                } else if (input_M>amount) {
                    printf("M coule not bigger than amount of records.\n");
                    printf("Please re-input M:");
                    scanf("%d", &input_M);
                }
            } while ((input_M<=0) || (input_M>amount));
            M_highest_salary(input_M);
            f_menu.status = _f_main_;
            break;
        case _all_matching_last_name_:
            lookup_all_lastname(f_menu.buf);
            f_menu.status = _f_main_;
            break;
        case _error_:
            break;
    }
}


