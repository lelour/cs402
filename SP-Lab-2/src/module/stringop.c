//
// Created by Jian Li on 2021/3/24.
//
// string related functions
#include "../../include/stringop.h"

// compare two string with case insensitive
// return: 0 - equal, 1 - str1>str2 , -1 - str1<str2
int stricmp(char str1[],char str2[]) {
    unsigned char chr1,chr2;
    int i=0;
    while(1)
    {
        chr1=(str1[i]>='a' && str1[i]<='z')?(str1[i]-32):str1[i];
        chr2=(str2[i]>='a' && str2[i]<='z')?(str2[i]-32):str2[i];
        i++;

        if(chr1!=chr2) break;
        if(chr1=='\0' || chr2=='\0') break;
    }
    if(chr1>chr2) return 1;
    else if(chr1==chr2) return 0;
    else return -1;
}


