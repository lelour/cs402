//
// Created by Jian Li on 2021/3/10.
//

#ifndef SP_LAB_1_READFILE_H
#define SP_LAB_1_READFILE_H

#define MAXEMPLOYEE 1024  // max employee
#define MAXNAME 64  // max name length
#define MAXLINELENGTH 200 // max length per line in database file

struct EMPLOYEE {
    int six_digit_ID;
    char first_name[MAXNAME];
    char last_name[MAXNAME];
    int salary;
};

int read_file(char *filename);

#endif //SP_LAB_1_READFILE_H
