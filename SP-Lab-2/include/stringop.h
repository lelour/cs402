//
// Created by Jian Li on 2021/3/24.
//

#ifndef SP_LAB_2_STRINGOP_H
#define SP_LAB_2_STRINGOP_H

// the same as strcasecmp in linux
// or stricmp in windows
// i create this function myself to fit both linux and windows
// compare two string with case insensitive
// return: 0 - equal, 1 - str1>str2 , -1 - str1<str2
int stricmp(char str1[],char str2[]);

#endif //SP_LAB_2_STRINGOP_H
