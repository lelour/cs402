//
// Created by Jian Li on 2021/3/16.
//

#ifndef SP_LAB_1_DBOP_H
#define SP_LAB_1_DBOP_H

// linked list
struct top_salary {
    int index;
    int salary;
    struct top_salary *next;
};

int sort_by_ID();
int print_database();
int lookup_id(int input_id);
int lookup_lastname(char *input_name);
int add_record(char *firstname, char *lastname, int salary);
int remove_id(int input_id);
int update_id(int input_id);
int M_highest_salary(int M);
int lookup_all_lastname(char *input_name);

#endif //SP_LAB_1_DBOP_H
