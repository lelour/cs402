//
// Created by Jian Li on 2021/3/10.
//
// Menu operation
//
#include <stdio.h>
#include <stdlib.h>
#include "../../include/menu.h"
#include "../../include/readfile.h"
#include "../../include/dbop.h"

extern struct EMPLOYEE employee[MAXEMPLOYEE]; // array to hold all records
extern int amount;  // amount of employee

// initialize the menu parameters
void init_menu() {
    f_menu.status = _f_main_;
    f_menu.buf_size = BUFFERSIZE;   // bytes get from stdio one time
    f_menu.buf = malloc(BUFFERSIZE);
}

// display menu
void display_menu() {
    switch(f_menu.status) {
        case _f_main_ :
            printf("Employee DB Menu:\n");
            printf("----------------------------------\n");
            printf("  (1) Print the Database\n");
            printf("  (2) Lookup by ID\n");
            printf("  (3) Lookup by Last Name\n");
            printf("  (4) Add an Employee\n");
            printf("  (5) Quit\n");
            printf("----------------------------------\n");
            printf("Enter your choice: ");
            break;
        case _lookup_id_ :
            printf("Enter a 6 digit employee id: ");
            break;
        case _lookup_last_name_ :
            printf("Enter Employee's last name (no extra spaces): ");
            break;
        case _add_ :
            printf("Enter the first name of the employee: ");
            break;
        case _quit_ :
            printf("goodbye!\n");
            exit(1);
            break;
        case _error_ :
            printf("Enter your choice: ");
            f_menu.status = _f_main_;
            break;
    }
}

// operations on menu choice
void do_menu(char *filename) {
    scanf("%s", f_menu.buf);
    // user input stored in buf
    switch (f_menu.status) {
        int choice;
        int input_id;
        char input_lastname[MAXNAME];
        int input_salary;
        int add_choice;
        int first_time_input;  // check input coorection
        int ret;

        case _f_main_ :
            sscanf(f_menu.buf, "%d", &choice);
            if (choice == 1) {
                print_database(filename);
            } else if (choice == 2) {
                f_menu.status = _lookup_id_;
            } else if (choice == 3) {
                f_menu.status = _lookup_last_name_;
            } else if (choice == 4) {
                f_menu.status = _add_;
            } else if (choice == 5) {
                f_menu.status = _quit_;
            } else {
                printf("Hey, %d is not between 1 and 5, try again...\n", choice);
                f_menu.status = _error_;
            }
            break;
        case _lookup_id_ :
            sscanf(f_menu.buf, "%d", &input_id);
            lookup_id(input_id);
            f_menu.status = _f_main_;
            break;
        case _lookup_last_name_ :
            lookup_lastname(f_menu.buf);
            f_menu.status = _f_main_;
            break;
        case _add_ :
            printf("Enter the last name of the employee: ");
            scanf("%s", input_lastname);
            first_time_input = 0;
            // salary must be in range
            do {
                if (first_time_input == 1) printf("Please check you input!\n");
                printf("Enter employee's salary (30000 to 150000): ");
                scanf("%d", &input_salary);
                if ((input_salary<30000) || (input_salary>150000)) first_time_input=1;
            } while(input_salary<30000 || input_salary>150000);
            printf("do you want to add the following employee to the DB?\n");
            printf("        %s %s, salary: %d\n", f_menu.buf, input_lastname, input_salary);
            printf("Enter 1 for yes, 0 for no: ");
            scanf("%d", &add_choice);
            if (add_choice == 1) {
                ret = add_record(f_menu.buf, input_lastname, input_salary);
                if (ret == -1) {
                    printf("Add record fail because ID cound not bigger than 999999!\n");
                }
            }
            f_menu.status = _f_main_;
            break;
        case _quit_:
            break;
        case _error_:
            break;
    }
}


