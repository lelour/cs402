//
// Created by Jian Li on 2021/3/16.
//
// database related ops
// split, read_int, read_string, read_float, read_record

#include <string.h>
#include <stdio.h>
#include "../../include/readfile.h"

extern struct EMPLOYEE employee[MAXEMPLOYEE]; // array to hold all records
extern int amount;  // amount of employee

// sort by ID in struct array
// return: 0 - success
int sort_by_ID() {
    struct EMPLOYEE tmp_emp;  // for swap
    // Bubble Sort by ID
    for (int i=0; i<amount-1; i++) {
        for (int j=0; j<amount-1-i; j++) {
            if (employee[j].six_digit_ID > employee[j+1].six_digit_ID) {
                tmp_emp = employee[j];
                employee[j] = employee[j+1];
                employee[j+1] = tmp_emp;
            }
        }
    }

    return 0;
}

// print all data of database file
// print format should be four columns matches lab demands
int print_database() {

    printf("\n");
    printf("NAME                              SALARY             ID\n");
    printf("---------------------------------------------------------------\n");
    for (int i=0; i<amount; i++) {
        printf("%-13s", employee[i].first_name);
        printf("%-21s", employee[i].last_name);
        printf("%6d", employee[i].salary);
        printf("%15d", employee[i].six_digit_ID);
        printf("\n");
    }
    printf("---------------------------------------------------------------\n");
    printf(" Number of Employees (%d)\n\n", amount);

    return 0; // 0 - success
}


// lookup record with id in memory
// when found there is only one result
// print format should be four columns matches lab demands
int lookup_id(int input_id) {
    struct EMPLOYEE current_emp; // current employee
    int found = 0;  // 0 - not found, 1 - found

    // linear search
    for (int i=0; i<amount; i++) {
        if (employee[i].six_digit_ID == input_id) {
            current_emp = employee[i];
            found = 1;
            break;
        }
    }

    if (found == 0) {
        printf("Employee with id %d not found in DB\n\n", input_id);
    } else {
        printf("\n");
        printf("NAME                              SALARY             ID\n");
        printf("---------------------------------------------------------------\n");
        printf("%-13s", current_emp.first_name);
        printf("%-21s", current_emp.last_name);
        printf("%6d", current_emp.salary);
        printf("%15d", current_emp.six_digit_ID);
        printf("\n");
        printf("---------------------------------------------------------------\n\n");

    }

    return 0; // 0 - success
}

// lookup record with lastname in memory
// when found print the first one
// print format should be four columns matches lab demands
int lookup_lastname(char *input_name) {
    struct EMPLOYEE current_emp; // current employee
    int found = 0;  // 0 - not found, 1 - found

    for (int i=0; i<amount; i++) {
        if (strcmp(employee[i].last_name, input_name) == 0) {
            current_emp = employee[i];
            found = 1;
            break;
        }
    }

    if (found == 0) {
        printf("Employee with lastname %s not found in DB\n\n", input_name);
    } else {
        printf("\n");
        printf("NAME                              SALARY             ID\n");
        printf("---------------------------------------------------------------\n");
        printf("%-13s", current_emp.first_name);
        printf("%-21s", current_emp.last_name);
        printf("%6d", current_emp.salary);
        printf("%15d", current_emp.six_digit_ID);
        printf("\n");
        printf("---------------------------------------------------------------\n\n");
    }

    return 0; // 0 - success
}

// append record to EOF in memory
int add_record(char *firstname, char *lastname, int salary) {

    int biggest_id = 0;

    // find the biggest ID
    for (int i=0; i<amount; i++) {
        if (employee[i].six_digit_ID > biggest_id) {
            biggest_id = employee[i].six_digit_ID;
        }
    }

    // ID must be in range, or it would not append record
    biggest_id++;  // id = biggest_id + 1
    if (biggest_id > 999999) {
        printf("Index is out of range [100000 - 999999]!\n");
        return -1;  // return fail
    }

    if (biggest_id < 100000) biggest_id = 100000;  // set to the least id of range when no data in file

    employee[amount].six_digit_ID = biggest_id;
    strcpy(employee[amount].first_name, firstname);
    strcpy(employee[amount].last_name, lastname);
    employee[amount].salary = salary;
    amount++;

    printf("\n");

    return 0; // 0 - success, -1 - fail
}


