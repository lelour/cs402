//
// Created by Jian Li on 2021/3/10.
//

#ifndef SP_LAB_1_MENU_H
#define SP_LAB_1_MENU_H

// menu input buffersize
#define BUFFERSIZE 256
// menu_item
enum menu_item {_f_main_, _lookup_id_, _lookup_last_name_, _add_, _quit_, _error_};

static struct {
    enum menu_item status;
    int buf_size;   // bytes get from stdio
    char *buf;      // address of bytes get from stdio
}f_menu;

void init_menu();
void display_menu();
void do_menu();

#endif //SP_LAB_1_MENU_H
