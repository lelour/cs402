//
// Created by Jian Li on 2021/3/16.
//

#ifndef SP_LAB_1_DBOP_H
#define SP_LAB_1_DBOP_H

int sort_by_ID();
int print_database();
int lookup_id(int input_id);
int lookup_lastname(char *input_name);
int add_record(char *firstname, char *lastname, int salary);

#endif //SP_LAB_1_DBOP_H
