How to use:
Program will take one command line argument, which is the name of the
input file containing employee information.
For example, telling your program to load the employee data stored
in the file "small.txt" would look like:
./workerDB small.txt


Directory Architecture:
include : contains all header files
src : contains C file
src/module: contains menu, read file, dbop three module C files.


System Architecture:
Modules:
  1, "main.c" is the entrance of this project and will load the command line parameter, check exist and format and sort in memory. Also there's an entry for menu.
  2, "menu.c" and "menu.h" are the menu display and operation handler
  3, "readfile.c" and "readfile.h" will handle file related ops, like open file, close file, read records in database file
  4, "dbop.c" and "dbop.h" hold database related logic ops, like sort, print database, lookup id, lookup last name and add record in memory


How to build:
make workersDB
make clean

After build, there will be a new file named workersDB.
To run it, just type:
./workersDB small.txt


The result is as shown below:
Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
----------------------------------
Enter your choice: 1

NAME                              SALARY             ID
---------------------------------------------------------------
Cathryn      Danner                72000         165417
Matt         Meeden                69000         273225
Robert       Dufour                91000         471163
Dylan        Steinberg             84000         485913
Mike         Griffin               72000         499959
Daniel       McNamee               71000         547935
Peter        Olsen                 82000         553997
Martine      Marshall              99000         633976
Jean         Jones                 94000         702234
Dana         Parrish               87000         784372
Ann          Coddington            82000         786785
Melissa      Dufour               114000         849387
Heather      James                 98000         935460
Russ         Vorobiev             109000         970016
---------------------------------------------------------------
 Number of Employees (14)

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
----------------------------------
Enter your choice: 2
Enter a 6 digit employee id: 935461
Employee with id 935461 not found in DB

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
----------------------------------
Enter your choice: 2
Enter a 6 digit employee id: 935460

NAME                              SALARY             ID
---------------------------------------------------------------
Heather      James                 98000         935460
---------------------------------------------------------------

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
----------------------------------
Enter your choice: 3
Enter Employee's last name (no extra spaces): James

NAME                              SALARY             ID
---------------------------------------------------------------
Heather      James                 98000         935460
---------------------------------------------------------------

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
----------------------------------
Enter your choice: 4
Enter the first name of the employee: Cobe
Enter the last name of the employee: Bryan
Enter employee's salary (30000 to 150000): 140000
do you want to add the following employee to the DB?
        Cobe Bryan, salary: 140000
Enter 1 for yes, 0 for no: 1

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
----------------------------------
Enter your choice: 1

NAME                              SALARY             ID
---------------------------------------------------------------
Cathryn      Danner                72000         165417
Matt         Meeden                69000         273225
Robert       Dufour                91000         471163
Dylan        Steinberg             84000         485913
Mike         Griffin               72000         499959
Daniel       McNamee               71000         547935
Peter        Olsen                 82000         553997
Martine      Marshall              99000         633976
Jean         Jones                 94000         702234
Dana         Parrish               87000         784372
Ann          Coddington            82000         786785
Melissa      Dufour               114000         849387
Heather      James                 98000         935460
Russ         Vorobiev             109000         970016
Cobe         Bryan                140000         970017
---------------------------------------------------------------
 Number of Employees (15)

Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
----------------------------------
Enter your choice: 5
goodbye!

