import pandas as pd

file1 = "tex.din"
file2 = "cc1.din"

def file_read(file):
    df = pd.read_table(file, header=None, delim_whitespace=True)
    df[1] = df[1].apply(lambda x: int(x, 16))
    return df

df1 = file_read(file1)
df2 = file_read(file2)

print('tex.bin')
print(df1.groupby(0).count())
print(df1.count())
print(df1.groupby(0).count()/df1.count())

print('cc1.bin')
print(df2.groupby(0).count())
print(df2.count())
print(df2.groupby(0).count()/df2.count())