import random
import time

m = 500  # rows
n = 400  # columns

# integer matrix
t0 = time.process_time()
for cnt in range(5):
    MatL = []
    for i in range(0, m):
        MatL.append([])
    for i in range(0, m):
        for j in range(0, n):
            MatL[i].append(j)
            MatL[i][j] = 0
            MatL[i][j] = random.randint(1, 1000)

    MatR = []
    for i in range(0, n):
        MatR.append([])
    for i in range(0, n):
        for j in range(0, m):
            MatR[i].append(j)
            MatR[i][j] = 0
            MatR[i][j] = random.randint(1, 1000)

    c = []
    for i in range(len(MatL)):  # rows of matrix
        c.append([])
        for j in range(len(MatR[0])):  # columns of matrix
            s = sum(MatL[i][k] * MatR[k][j] for k in range(len(MatR)))  # dot product
            c[i].append(s)

t1 = time.process_time()
print('integer matrix average process time = ', (t1 - t0) / 5.0)

# real matrix, in PYTHON real type is FP32, equal to double in JAVA
t0 = time.process_time()
for cnt in range(5):
    MatL = []
    for i in range(0, m):
        MatL.append([])
    for i in range(0, m):
        for j in range(0, n):
            MatL[i].append(j)
            MatL[i][j] = 0
            MatL[i][j] = random.uniform(1, 1000)

    MatR = []
    for i in range(0, n):
        MatR.append([])
    for i in range(0, n):
        for j in range(0, m):
            MatR[i].append(j)
            MatR[i][j] = 0
            MatR[i][j] = random.uniform(1, 1000)

    c = []
    for i in range(len(MatL)):  # rows of matrix
        c.append([])
        for j in range(len(MatR[0])):  # columns of matrix
            s = sum(MatL[i][k] * MatR[k][j] for k in range(len(MatR)))  # dot product
            c[i].append(s)

t1 = time.process_time()
print('real matrix average process time = ', (t1 - t0) / 5.0)