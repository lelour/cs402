import matplotlib.pyplot as plt
import pandas as pd

file1 = "tex.din"
file2 = "cc1.din"

def file_read(file):
    df = pd.read_table(file, header=None, delim_whitespace=True)
    df[1] = df[1].apply(lambda x: int(x, 16))
    return df

df1 = file_read(file1)
df2 = file_read(file2)

print(df1.describe())
plt.title('tex.din')
df1[1].plot.hist(bins=100, grid=True)
plt.xlabel('Ox')
plt.ylabel('Oy')
plt.show()

plt.title('tex.din')
df1[1].plot.hist(bins=100, range=(4394092, 4394192), grid=True)
plt.xlabel('Ox')
plt.ylabel('Oy')
plt.show()

print(df2.describe())
plt.title('cc1.din')
df2[1].plot.hist(bins=100, grid=True)
plt.xlabel('Ox')
plt.ylabel('Oy')
plt.show()

plt.title('cc1.din')
df2[1].plot.hist(bins=100, range=(4194784, 4194884), grid=True)
plt.xlabel('Ox')
plt.ylabel('Oy')
plt.show()



