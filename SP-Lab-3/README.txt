How to use:
Program will take one command line argument, which is the name of the
input file containing datum information.
For example, telling your program to load the datum stored
in the file "small.txt" would look like:
./basicstats small.txt


Directory Architecture:
include : contains all header files
src : contains C file
src/module: contains readfile, statisticop, arrayop three module C files.


System Architecture:
Modules:
  1, "main.c" is the entrance of this project and will load the command line parameter, check exist and format and dynamically load datun into memory.
  2, "readfile.c" and "readfile.h" will handle file related ops, like open file, close file, dynamically read records into memory. And will expand heap size when need.
  3, "statisticop.c" and "statisticop.h" will compute mean, median, stddev of array and print out
  4, "arrayop.c" and "arrayop.h" hold dynamic array related ops, include sort and expand array.


How to build:
make basicstats
make clean

After build, there will be a new file named basicstats.
To run it, just type:
./basicstats small.txt


The result is as shown below:
Results:
--------
Num values:            12
      mean:        85.776
    median:        67.470
    stddev:        90.380
Unused array capacity: 8