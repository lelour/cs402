//
// Created by Jian Li on 2021/4/6.
//
// File operation:
// file exist, readfile

#include <stdio.h>
#include <stdlib.h>
#include "../../include/readfile.h"
#include "../../include/arrayop.h"

// local viarable used in this library
FILE *fp;

// open file with filename and mode
int open_file(char *filename) {
    fp = fopen(filename, "r");
    if (fp != NULL) {
        return 0;  // exist
    } else {
        return -1; // not exist
    }
}

// close file handler
int close_file() {
    fclose(fp);
    return 0;
}

// whether string is number, include .
// return: 0=is number, -1=not a bumber
int isNumber(char str[]) {
    char* tmp = str;
    int flag = 0;  // indicate whether a number

    while(*tmp) {
        if (!((*tmp>='0' && *tmp<='9') || (*tmp=='.'))) {
            flag = -1;
            break;
        }
        ++tmp;
    }
    return flag;
}

// I implement below function read_file with return type is int, not as demand type float *
// Because with this type, it could include more informations, like file not exist , format mismatch or memory allocation error
// but with return type "float *", these important informations will be lost!
// or the code structure will be much complex.
// And with general scenario, this type is much widely used and much reasonable than float *
// **************
// read records in datum file
// format should be float
// data parsed into dynamic array data
// return:
// -1  file not exist
// -2  file format mismatch
// -3  memory allocation error
//  0  success
int read_file(char *filename, float **data, int *totallength, int *validlength) {
    int ret;  // call function return value
    int index = 0;  // index of current pointer of array
    char fValue[100];  // read string into memory
    float record;  // convert string to float to reduce float reading error
    int isnumber;  // check whether is number
    void *addr;

    ret = open_file(filename);
    if (ret == -1) return -1;  // file not exist

    while (1==1) {
        // read string into memory instead of float to reduce float reading error
        fscanf(fp, "%s", fValue);
        // to avoid last line releat read
        if (feof(fp))
            break;
        isnumber = isNumber(fValue);  // make sure can be convert into float
        if ( isnumber == 0) {
            record = atof(fValue);
        } else {
            return -2;  // format mismatch
        }
        // whether should expand dynamic array
        if (index == *totallength) {
            ret = expand_array(data, totallength);
            if (ret == -1) {
                return -3;
            }
        }

        // [] is higher precedence than *, so should use (*)[] here
        (*data)[index] = record;
        index++;
    }
    close_file();

    // valid length
    *validlength = index;

    return 0; // 0 - success
}


