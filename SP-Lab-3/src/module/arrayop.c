//
// Created by Jian Li on 2021/4/6.
//
// related ops with dynamic array
// e.g. sort, expand array
#include <stdlib.h>

// selection sort
// return: 0 - correct
int array_sort(float *data, int validlength) {
    int minIndex;
    float tmp;

    for (int i=0; i<validlength; i++) {
        minIndex = i;
        for (int j=i+1; j<validlength; j++) {
            if (data[j] < data[minIndex]) {
                minIndex = j;
            }
        }
        if (minIndex != i) {
            tmp = data[i];
            data[i] = data[minIndex];
            data[minIndex] = tmp;
        }
    }

    return 0;
}

// dynamic expand memory array
// if array is big enough and memory is not insufficient, there will be a memory allocation error
// return: 0 = correct, -1 = memory allocation error
int expand_array(float **data, int *totallength) {
    float *newdata;

    // allocate new space 2 times than old one
    newdata = (float *)malloc(sizeof(float)*2*(*totallength));

    // check whether memory allocation error
    if (newdata == NULL) {
        return -1;
    }

    for (int i=0; i<*totallength; i++) {
        newdata[i] = (*data)[i];
    }
    // free old space
    free(*data);
    // change **pointer to new space
    *data = newdata;

    // expand length 2 times
    *totallength = 2*(*totallength);

    return 0;
}

