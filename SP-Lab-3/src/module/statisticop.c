//
// Created by Jian Li on 2021/4/6.
//
// compute statistic result and print

#include <stdio.h>
#include <math.h>
#include "../../include/statisticop.h"
#include "../../include/arrayop.h"

// compute mean of array
// as demand, return type is double
double mean(float *data, int validlength) {
    double result = 0.0;  //initial

    for (int i=0; i<validlength; i++) {
        result += data[i];
    }
    result = result / validlength;

    return result;
}

// compute median of array
float median(float *data, int validlength) {
    // sort first
    array_sort(data, validlength);

    // even/odd length
    if (validlength % 2 == 0)
        return ((data[validlength/2] + data[validlength/2-1]) / 2);
    return data[validlength/2];
}

// compute stddev of array
// as demand, return type is double
double stddev(float *data, int validlength) {
    double array_mean;
    float step_result;
    float sum = 0;

    // get mean of array
    array_mean = mean(data, validlength);

    // sum(data[i]-mean)^2)
    for (int i=0; i<validlength; i++) {
        step_result = data[i] - array_mean;
        sum += step_result * step_result;
    }

    return sqrt(sum / validlength);

}

// output statistic results
void stastic_array(float *data, int totallength, int validlength) {
    printf("Results:\n");
    printf("--------\n");
    printf("Num values:%14d\n", validlength);
    printf("      mean:%14.3f\n", mean(data, validlength));
    printf("    median:%14.3f\n", median(data, validlength));
    printf("    stddev:%14.3f\n", stddev(data, validlength));
    printf("Unused array capacity: %d\n", totallength-validlength);
}

