//
// Created by Jian Li on 2021/4/5.
//
// Purpose:
// Ths project is to implement statistic of mean, medium, stddev of a file with decimal datum.
//
// How to use:
// Program will take one command line argument, which is the name of the
// input file containing datum information.
// For example, telling your program to load the datum stored
// in the file "small.txt" would look like:
//./basicstats small.txt
//
//Modules:
// main.c is the entrance of this project and will load the command line parameter, check exist and format and dynamically load datun into memory.
// readfile.c will handle file related ops, like open file, close file, dynamically read records into memory. And will expand heap size when need.
// statisticop.c will compute mean, median, stddev of array and print out
// arrayop.c holds dynamic array related ops, include sort and expand array.
//
#include <stdio.h>
#include <stdlib.h>
#include "../include/readfile.h"
#include "../include/statisticop.h"

// main entry of this program
int main(int argc, char *argv[]) {
    float *data = NULL;  // dynamic array of float datas
    int totallength = 0;  // length of this array
    int validlength = 0;  // valid datum length of this array
    char *dbfile;
    // dbfile = "small.txt";    // for test to obtain dbfile
    // dbfile = "large.txt";    // for test to obtain dbfile


    // get parameter as database filename
    if (argc != 2) {
        printf("The command format should be (e.g.) : ./basicstats small.txt\n");
        return -1;  //abnormal exit
    } else {
        dbfile = argv[1];
    }

    // initial heap array
    if (data == NULL) {
        data = (float *)malloc(sizeof(float)*ARRAY_LENGTH);
        totallength = ARRAY_LENGTH;
    }

    if (data == NULL) {
        printf("Error when heap memory allocation.");
        return -1;
    }

    // read records in dbfile
    // ret:
    // 0 success, -1 not exist, -2 format error
    int ret = read_file(dbfile, &data, &totallength, &validlength);

    if (ret == -1) {
        printf("%s does not exist!\n", dbfile);
        return -1;
    }
    if (ret == -2) {
        printf("%s format is incorrect! Please verify.\n", dbfile);
        return -1;
    }

    // print statistic results
    stastic_array(data, totallength, validlength);

    // free all dynamic spaces
    free(data);

    return 0;  // normal exit
}


