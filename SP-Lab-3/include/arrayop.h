//
// Created by Jian Li on 2021/4/6.
//

#ifndef SP_LAB_3_ARRAYOP_H
#define SP_LAB_3_ARRAYOP_H

// selection sort
int array_sort(float *data, int validlength);
// dynamic expand array
int expand_array(float **data, int *totallength);

#endif //SP_LAB_3_ARRAYOP_H
