//
// Created by Jian Li on 2021/4/6.
//

#ifndef SP_LAB_3_READFILE_H
#define SP_LAB_3_READFILE_H

// initial array length
#define ARRAY_LENGTH 20

// read file named filename and put float data into dynamic array data
// length is the length of this array
// return: 0 = success , -1 = file not exist, -2 = format is incorrect
int read_file(char *filename, float **data, int *totallength, int *validlength);

#endif //SP_LAB_3_READFILE_H
